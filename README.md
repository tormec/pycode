# PyCode #

PyCode is a plugin for DokuWiki. 

It allows you to embeds a Python script hosted in a remote repository such as Bitbucket or GitHub.

For more information and how to install it, please visit https://www.dokuwiki.org/plugin:pycode.