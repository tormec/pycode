/**
 * PyCode plugin: it embeds a Python script hosted in a remote repository.
 *
 * script.js: it defines the PyCode Wizard.
 *
 * @author Torpedo <dgtorpedo@gmail.com>
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @package script
 */

var pycode_wiz = {
    $wiz: null,
    timer: null,
    textArea: null,

    /**
     * Initialize the pycode_wiz by creating the needed HTML
     * and attaching the eventhandlers.
     */
    init: function($editor){
        // position relative to the text area
        var pos = $editor.position();

        // create HTML Structure
        pycode_wiz.$wiz = jQuery(document.createElement("div"))
            .dialog({
                autoOpen: false,
                draggable: true,
                title: LANG.plugins.pycode.wizard,
                resizable: false
            })
            .html(
                '<fieldset class="index">' +
                    '<legend>'+LANG.plugins.pycode.repository+'</legend>' +
                    '<div class="pycode__repo-src-url">' +
                        '<button id="pycode__b" type="button">' +
                            '<img src="'+DOKU_BASE+'lib/plugins/pycode/images/b.png"></img>' +
                         '</button>' +
                        '<button id="pycode__g" type="button">' +
                            '<img  src="'+DOKU_BASE+'lib/plugins/pycode/images/g.png"></img>' +
                         '</button>' +
                        '<input id="pycode__src-url" type="text" disabled>' +
                    '</div>' +
                '</fieldset>' +

                '<fieldset class="options">' +
                    '<legend>'+LANG.plugins.pycode.options+'</legend>' +
                    '<div>' +
                        '<strong>'+LANG.plugins.pycode.embed+'</strong><br />' +

                        '<div class="pycode__embed-lns" id="pycode__left">' +
                            '<label><input id="pycode__opt-lns" type="checkbox">' +
                            LANG.plugins.pycode.lns+'</label>' +
                        '</div>' +
                        '<div class="pycode__embed-lns" id="pycode__right">' +
                            '<input id="pycode__lns-from" type="text" disabled>' +
                            ' : ' +
                            '<input id="pycode__lns-to" type="text" disabled>' +
                        '</div>' +

                        '<div class="pycode__embed-def" id="pycode__left">' +
                            '<label><input id="pycode__opt-def" type="checkbox">' +
                            LANG.plugins.pycode.def+'</label>' +
                        '</div>' +
                        '<div class="pycode__embed-def" id="pycode__right">' +
                            '<input id="pycode__def" type="text" disabled>' +
                        '</div>' +

                        '<div class="pycode__embed-cls" id="pycode__left">' +
                            '<label><input id="pycode__opt-cls" type="checkbox">' +
                            LANG.plugins.pycode.cls+'</label>' +
                        '</div>' +
                        '<div class="pycode__embed-cls" id="pycode__right">' +
                            '<input id="pycode__cls" type="text" disabled>' +
                        '</div>' +
                    '</div>' +

                    '<div>' +
                        '<strong>'+LANG.plugins.pycode.nums+'</strong><br />' +

                        '<div class="pycode__code-nums" id="pycode__left">' +
                            '<label><input id="pycode__opt-nums" type="checkbox">' +
                            LANG.plugins.pycode.change+'</label>' +
                        '</div>' +
                        '<div class="pycode__code-nums" id="pycode__right">' +
                            '<label><input type="radio" name="radio-nums" value="show" disabled>' +
                            LANG.plugins.pycode.show+'</label>' +
                            '<label><input type="radio" name="radio-nums" value="hide" disabled>' +
                            LANG.plugins.pycode.hide+'</label>' +
                        '</div>' +
                    '</div>' +

                    '<div>' +
                        '<strong>'+LANG.plugins.pycode.title+'</strong><br />' +

                        '<div class="pycode__code-title" id="pycode__left">' +
                            '<label><input id="pycode__opt-title" type="checkbox">' +
                            LANG.plugins.pycode.change+'</label>' +
                        '</div>' +
                        '<div class="pycode__code-title" id="pycode__right">' +
                            '<input id="pycode__title" type="text" disabled>' +
                        '</div>' +
                    '</div>' +

                    '<div>' +
                        '<strong>'+LANG.plugins.pycode.docstr+'</strong><br />' +

                        '<div class="pycode__code-docstr" id="pycode__left">' +
                            '<label><input id="pycode__opt-docstr" type="checkbox">' +
                            LANG.plugins.pycode.change+'</label>' +
                        '</div>' +
                        '<div class="pycode__code-docstr" id="pycode__right">' +
                            '<label><input type="radio" name="radio-docstr" value="show" disabled>' +
                            LANG.plugins.pycode.show+'</label>' +
                            '<label><input type="radio" name="radio-docstr" value="hide" disabled>' +
                            LANG.plugins.pycode.hide+'</label>' +
                        '</div>' +
                    '</div>' +
                '</fieldset>' +
                '<input type="submit" value="'+LANG.plugins.pycode.insert+'" id="pycode__insert">'
            )
            .parent()
            .attr("id","pycode__wiz")
            .css({
                "position": "absolute",
                "top": (pos.top+20)+"px",
                "left": (pos.left+80)+"px"
            })
            .hide()
            .appendTo(".dokuwiki:first");

        pycode_wiz.textArea = $editor[0];

        // start to attach event handler

        // the following is the table of all possibiles combinations,
        // depending on flag, file extension and options
        //
        //         |        .py        ||        .*         |
        //         --------------------||--------------------
        //         |   | l | f |f+c| c ||   | l | f |f+c| c |
        // ----------------------------||--------------------
        // nums    | V | V | V | V | X || V | V | X | X | X |
        // ----------------------------||--------------------
        // title   | V | V | V | V | X || V | V | X | X | X |
        // ----------------------------||--------------------
        // docstr  | X | X | X | X | V || X | X | X | X | X |
        // ----------------------------||--------------------

        // handle buttons for the repository
        jQuery("#pycode__b").click(function() {
            pycode_wiz.reset(".pycode__repo-src-url");
            jQuery("#pycode__b").fadeTo(0, 1);
            jQuery("#pycode__g").fadeTo(0, 0.5);
            if (jQuery("#pycode__src-url").prop("disabled") == true) {
                jQuery("#pycode__src-url").prop("disabled", false);
            }
            jQuery("#pycode__src-url")
            .val("https://bitbucket.org/<user>/<repo>/src/<branch>/<file>")
            .click(function() {pycode_wiz.reset(".pycode__repo-src-url");});
        });
        jQuery("#pycode__g").click(function() {
            pycode_wiz.reset(".pycode__repo-src-url");
            jQuery("#pycode__g").fadeTo(0, 1);
            jQuery("#pycode__b").fadeTo(0, 0.5);
            if (jQuery('#pycode__src-url').prop("disabled") == true) {
                jQuery('#pycode__src-url').prop("disabled", false);
            }
            jQuery("#pycode__src-url")
            .val("https://github.com/<user>/<repo>/blob/<branch>/<file>")
            .click(function() {pycode_wiz.reset(".pycode__repo-src-url");});
        });

        // handle checkbox for embed code between two line numbers
        jQuery("#pycode__opt-lns").change(function() {
            if (jQuery("#pycode__opt-lns").prop("checked") == true) {
                jQuery(".pycode__embed-lns input:text").prop("disabled", false);
                pycode_wiz.disable(".pycode__embed-def");
                pycode_wiz.disable(".pycode__embed-cls");
                pycode_wiz.disable(".pycode__code-docstr");
                pycode_wiz.reset(".pycode__embed-def");
                pycode_wiz.reset(".pycode__embed-cls");
                pycode_wiz.reset(".pycode__code-docstr");
            }
            else {
                pycode_wiz.disable(".pycode__embed-lns");
            }
        });

        // handle checkbox for embed code from function name
        jQuery("#pycode__opt-def").change(function() {
            if (jQuery("#pycode__opt-def").prop("checked") == true) {
                jQuery("#pycode__def")
                .prop("disabled", false)
                .click(function() {pycode_wiz.reset(".pycode__embed-def");});
                pycode_wiz.disable(".pycode__embed-lns");
                pycode_wiz.disable(".pycode__code-docstr");
                pycode_wiz.reset(".pycode__code-docstr");
            }
            else {
                pycode_wiz.disable(".pycode__embed-def");
                pycode_wiz.reset(".pycode__embed-def");
                if (jQuery("#pycode__opt-cls").prop("checked") == true) {
                    pycode_wiz.disable(".pycode__code-nums");
                    pycode_wiz.disable(".pycode__code-title");
                    pycode_wiz.reset(".pycode__code-nums");
                }
            }
        });

        // handle checkbox for embed code from class name
        jQuery("#pycode__opt-cls").change(function() {
            if (jQuery("#pycode__opt-cls").prop("checked") == true) {
                jQuery("#pycode__cls")
                .prop("disabled", false)
                .click(function() {pycode_wiz.reset(".pycode__embed-cls");});
                pycode_wiz.disable(".pycode__embed-lns");
                if (jQuery("#pycode__opt-def").prop("checked") == false) {
                    pycode_wiz.disable(".pycode__code-nums");
                    pycode_wiz.disable(".pycode__code-title");
                    pycode_wiz.reset(".pycode__code-nums");
                }
            }
            else {
                pycode_wiz.disable(".pycode__embed-cls");
                pycode_wiz.disable(".pycode__code-docstr");
                pycode_wiz.reset(".pycode__embed-cls");
                pycode_wiz.reset(".pycode__code-docstr");
            }
        });

        // handle checkbox for show/hide line numbers
        jQuery("#pycode__opt-nums").change(function() {
            if (jQuery("#pycode__opt-nums").prop("checked") == true) {
                jQuery(".pycode__code-nums input:radio")
                .prop("disabled", false)
                .click(function() {pycode_wiz.reset('.pycode__code-nums');});
                if (jQuery("#pycode__opt-def").prop("checked") == false &&
                    jQuery("#pycode__opt-cls").prop("checked") == true) {
                    pycode_wiz.disable(".pycode__embed-cls");
                    pycode_wiz.disable(".pycode__code-docstr");
                    pycode_wiz.reset(".pycode__embed-cls");
                    pycode_wiz.reset(".pycode__code-docstr");
                }
            }
            else {
                pycode_wiz.disable(".pycode__code-nums");
                pycode_wiz.reset(".pycode__code-nums");
            }
        });

        // handle checkbox for define new title for the code
        jQuery("#pycode__opt-title").change(function() {
            if (jQuery("#pycode__opt-title").prop("checked") == true) {
                jQuery("#pycode__title").prop("disabled", false);
                if (jQuery("#pycode__opt-def").prop("checked") == false &&
                    jQuery("#pycode__opt-cls").prop("checked") == true) {
                    pycode_wiz.disable(".pycode__embed-cls");
                    pycode_wiz.disable(".pycode__code-docstr");
                    pycode_wiz.reset(".pycode__embed-cls");
                    pycode_wiz.reset(".pycode__code-docstr");
                }

            }
            else {
                pycode_wiz.disable(".pycode__code-title");
            }
        });

        // handle checkbox for show/hide docstring
        jQuery("#pycode__opt-docstr").change(function() {
            if (jQuery("#pycode__opt-docstr").prop("checked") == true) {
                jQuery(".pycode__code-docstr input:radio")
                .prop("disabled", false)
                .click(function() {pycode_wiz.reset('.pycode__code-docstr');});
                pycode_wiz.disable(".pycode__embed-lns");
                pycode_wiz.disable(".pycode__embed-def");
                pycode_wiz.disable(".pycode__code-nums");
                pycode_wiz.disable(".pycode__code-title");
                pycode_wiz.reset(".pycode__embed-def");
                pycode_wiz.reset(".pycode__code-nums");
            }
            else {
                pycode_wiz.disable(".pycode__code-docstr");
                pycode_wiz.reset(".pycode__code-docstr");
            }
        });

        // handle button for insert the syntax
        jQuery("#pycode__insert").click(pycode_wiz.insertPycode);

        // handle button for close the wizard
        jQuery("#pycode__wiz").find(".ui-dialog-titlebar-close").click(pycode_wiz.hide);
    },

    /**
     * Disable input objects belong to a given div.
     *
     * @param (obj) obj class name of a div
     */
    disable: function(obj) {
        jQuery(obj+" input:checkbox").prop("checked", false);
        jQuery(obj+" input:radio").prop("disabled", true);
        jQuery(obj+" input:text").prop("disabled", true);
    },

    /**
     * Set css rules defined for required fields missing.
     *
     * @param (obj) obj class name of a div
     */
    require: function(obj) {
        jQuery(obj+" input:radio").css("outline", "2px solid #FCC");
        jQuery(obj+" input:text").css("background-color", "#FCC");
    },

    /**
     * Reset css rules previously defined for required fields missing.
     *
     * @param (obj) obj class name of a div
     */
    reset: function(obj) {
        jQuery(obj+" input:radio").css("outline", "");
        jQuery(obj+" input:text").css("background-color", "");
    },

    /**
     * Get the extension of a given src-url.
     *
     * @param (str) src_url the url to the source code
     */
    getExtension: function(src_url) {
        var ext = src_url.slice(-3);  // get .py
        if (ext == ".py") {
            return "py";
        }
        else {
            return "no-py";
        }
    },

    /**
     * Insert the syntax into the textarea,
     * replacing the current selection or at the cursor position.
     */
    insertPycode: function() {
        var data = {
            "src_url": null,
            "flag": null,
            "name": null,
            "nums": null,
            "title": null,
            "docstr": null
        };
        var error = new Array;

        // get the values from the fields
        // and check if some data has been missed

        // get value from src-url field
        if (jQuery("#pycode__src-url").val() == "") {
            error.push("no-src-url");
        }
        else {
            data["src_url"] = jQuery("#pycode__src-url").val();
            if (pycode_wiz.getExtension(data["src_url"]) == "no-py" &&
                (jQuery("#pycode__opt-def").prop("checked") == true ||
                jQuery("#pycode__opt-cls").prop("checked") == true)) {
                error.push("no-py");
            }
        }

        // get value from name lines field
        if (jQuery("#pycode__opt-lns").prop("checked") == true) {
            data["flag"] = "l";
            data["name"] = jQuery("#pycode__lns-from").val()+":"+jQuery("#pycode__lns-to").val();
        }
        // get value from name function field
        else if ((jQuery("#pycode__opt-def").prop("checked") == true) &&
                 (jQuery("#pycode__opt-cls").prop("checked") == false)) {
            data["flag"] = "f";
            if (jQuery("#pycode__def").val() == "") {
                error.push("no-def");
            }
            else {
                data["name"] = jQuery("#pycode__def").val();
            }
        }
        // get value from names method and class fields
        else if ((jQuery("#pycode__opt-def").prop("checked") == true) &&
                 (jQuery("#pycode__opt-cls").prop("checked") == true)) {
            data["flag"] = "f";
            if (jQuery("#pycode__def").val() == "" &&
                jQuery("#pycode__cls").val() == "") {
                error.push("no-def-cls");
            }
            else if (jQuery("#pycode__def").val() != "" &&
                     jQuery("#pycode__cls").val() == "") {
                error.push("no-cls");
            }
            else if (jQuery("#pycode__def").val() == "" &&
                     jQuery("#pycode__cls").val() != "") {
                error.push("no-def");
            }
            else {
                data["name"] = jQuery("#pycode__def").val()+" "+jQuery("#pycode__cls").val();
            }
        }
        // get value from name class field
        else if ((jQuery("#pycode__opt-def").prop("checked") == false) &&
                 (jQuery("#pycode__opt-cls").prop("checked") == true)) {
            data["flag"] = "c";
            if (jQuery("#pycode__cls").val() != "") {
                data["name"] = jQuery("#pycode__cls").val();
            }
            else {
                error.push("no-cls");
            }
        }
        // embed the whole file
        else {
            data["flag"] = "";
        }


        // get value from line numbers option
        if (jQuery("#pycode__opt-nums").prop("checked") == true) {
            if (jQuery(".pycode__code-nums input:radio:checked").val() == "show") {
                data["nums"] = "-nums = 1";
            }
            else if (jQuery(".pycode__code-nums input:radio:checked").val() == "hide") {
                data["nums"] = "-nums = 0";
            }
            else {
                error.push("no-nums");
            }
        }

        // get value from title option
        if (jQuery("#pycode__opt-title").prop("checked") == true) {
            title = jQuery("#pycode__title").val();
            data["title"] = '-title = "'+title+'"';
        }

        // get value from docstring option
        if (jQuery("#pycode__opt-docstr").prop("checked") == true &&
            jQuery("#pycode__opt-cls").prop("checked") == true) {
            if (jQuery(".pycode__code-docstr input:radio:checked").val() == "show") {
                data["docstr"] = "-docstr = 1";
            }
            else if (jQuery(".pycode__code-docstr input:radio:checked").val() == "hide") {
                data["docstr"] = "-docstr = 0";
            }
            else {
                error.push("no-docstr");
            }
        }
        else if (jQuery("#pycode__opt-docstr").prop("checked") == true &&
                 jQuery("#pycode__opt-cls").prop("checked") == false) {
            error.push("no-cls");
        }

        if (error.length == 0) {
            // depending on the choices made, print the right syntax
            var syntax = "<pycode";
            for (var k in data) {
                if (data[k] != null) {
                    if (data[k] == "") {
                        syntax += data[k];
                    }
                    else {
                        syntax += " " + data[k];
                    }
                }
            }
            syntax += ">";
            // get current selection/cursor position in a given textArea
            // with DWgetSelection() defined in lib/scripts/textselection.js
            sel = DWgetSelection(pycode_wiz.textArea);
            // insert/replace given text at the current cursor position
            // with pasteText() defined in lib/scripts/textselection.js
            pasteText(sel, syntax, 0);
            // finally close the wizard
            pycode_wiz.hide();
        }
        else {
            // highlight missing fields
            error.forEach(function(item, index, array) {
                if (item == "no-src-url") {
                    pycode_wiz.require(".pycode__repo-src-url");
                }
                else if (item == "no-py") {
                    pycode_wiz.disable(".pycode__embed-def");
                    pycode_wiz.disable(".pycode__embed-cls");
                    pycode_wiz.reset(".pycode__embed-def");
                    pycode_wiz.reset(".pycode__embed-cls");
                }
                else if (item == "no-def") {
                    pycode_wiz.require(".pycode__embed-def");
                }
                else if (item == "no-def-cls") {
                    pycode_wiz.require(".pycode__embed-def");
                    pycode_wiz.require(".pycode__embed-cls");
                }
                else if (item == "no-cls") {
                    pycode_wiz.require(".pycode__embed-cls");
                }
                else if (item == "no-nums") {
                    pycode_wiz.require(".pycode__code-nums");
                }
                else if (item == "no-docstr") {
                    pycode_wiz.require(".pycode__code-docstr");
                }
            })
        }
    },

    /**
     * Show the pycode wizard
     */
    show: function() {
        pycode_wiz.selection = DWgetSelection(pycode_wiz.textArea);
        pycode_wiz.$wiz.show();
    },

    /**
     * Hide the pycode wizard
     */
    hide: function() {
        pycode_wiz.$wiz.hide();
        pycode_wiz.textArea.focus();
    },

    /**
     * Toggle the pycode wizard
     */
    toggle: function() {
        if (pycode_wiz.$wiz.css("display") == "none") {
            pycode_wiz.show();
        }
        else {
            pycode_wiz.hide();
        }
    }
};

/**
 * It adds button action for the toolbar button
 *
 * @param (obj) $btn jQuery button element to add the action to.
 * @param (arr) props Associative array of button properties.
 * @param (str) edid  ID of the editor textarea.
 * @return (str) If button should be appended, return the id for in
 *               aria-controls, otherwise an empty string.
 */
function addBtnActionPyCode($btn, props, edid) {
    pycode_wiz.init(jQuery('#' + edid));
    $btn.click(function () {
        pycode_wiz.toggle();
        return false;
    });
    return true;
}
