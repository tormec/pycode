base   pycode
author Torpedo
email  dgtorpedo@gmail.com
date   2019-11-25
name   PyCode Plugin
desc   Plugin to embed a Python script hosted in a remote repository.
url    https://www.dokuwiki.org/plugin:pycode
