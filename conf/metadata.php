<?php
/**
 * PyCode plugin: it embeds a Python script hosted in a remote repository.
 *
 * metadata.php: it defines the kinds of metadata used in
 *     the Configuration Manager.
 *
 * @author Torpedo <dgtorpedo@gmail.com>
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @package metadata
 */

// btns = (int) value of the checkbox:
//      0 = no buttons in the toolbar
//      1 = (default) show buttons
$meta["btns"] = array("onoff");

// "cache" = (int) value of the checkbox input:
//      0 = enable cache
//      1 = disable cache (default)
$meta["cache"] = array("onoff");

// "nums" = (int) value of the checkbox:
//      0 = show line numbers (default)
//      1 = hide line numbers
$meta["nums"] = array("onoff");

// "title" = (str) value of the combobox which is a combination of:
//      "none" = no title (default)
//      "class" = <class-name> (only for Python script)
//      "def" = <function-name> (only for Python script)
//      "file" = <file-name>
//      "src-url" = path to <file-name>
$meta["title"] = array("multichoice",
                       "_choices" => array("none",
                                           "file ⋅ class ⋅ def ⋅ #:#",
                                           "src-url ⋅ class ⋅ def ⋅ #:#"));

// "docstr" = (int) value of the checkbox:
//      0 = show docstring (default)
//      1 = hide docstring
$meta["docstr"] = array("onoff");
