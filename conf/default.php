<?php
/**
 * PyCode plugin: it embeds a Python script hosted in a remote repository.
 *
 * default.php: it defines the default settings setting up in
 *      the Configuration Manager.
 *
 * @author Torpedo <dgtorpedo@gmail.com>
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @package default_settings
 */

$conf["btns"] = 1;  // for default shortcut buttons are shown in the toolbar
$conf["cache"] = 1;  // for default the page is not cached
$conf["nums"] = 0;  // for default the line numbers are not shown
$conf["title"] = "none";  // for default the title is not shown
$conf["docstr"] = 0;  // for default the docstring is not shown
