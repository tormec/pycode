<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Torpedo <dgtorpedo@gmail.com>
 * @author Kiril <neohidra@gmail.com>
 */
$lang['btns']                  = 'Показване на бутон за бърз достъп в лентата с инструменти.';
$lang['cache']                 = 'Изключване на кеширането.';
$lang['nums']                  = 'Показване номерата на редове.';
$lang['title']                 = 'Показване заглавието на кода.';
$lang['docstr']                = 'Показване на докстринга.';
