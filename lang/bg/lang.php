<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Torpedo <dgtorpedo@gmail.com>
 * @author Kiril <neohidra@gmail.com>
 */
$lang['note']                  = 'Кодът е бил изменен след последното редактиране на страницата.';
$lang['notfound-lns']          = 'Линиите на кода които се опитвате да внесете са извън допустимите граници.';
$lang['notfound-def']          = 'Функцията която се опитвате да внесете не съществува.';
$lang['notfound-cls']          = 'Класът който се опитвате да внесете не съществува.';
$lang['wrong-flag']            = 'Показателят е неправилен.';
$lang['wrong-lang']            = 'Файлът който се опитвате да внесете не е Python скрипт.';
$lang['error']                 = 'Отварянето на изходния код се провали.
Моля проверете адреса на хранилището.';
$lang['pycode']                = 'PyCode приставка';
$lang['title']                 = 'Ново заглавие';
$lang['js']['wizard']          = 'PyCode съветник';
$lang['js']['repository']      = 'Хранилище';
$lang['js']['options']         = 'Настройки';
$lang['js']['embed']           = 'Вграден';
$lang['js']['lns']             = 'редове';
$lang['js']['def']             = 'функция';
$lang['js']['cls']             = 'клас';
$lang['js']['nums']            = 'Hомера на редове';
$lang['js']['docstr']          = 'Докстринг';
$lang['js']['show']            = 'покажи';
$lang['js']['hide']            = 'скрий';
$lang['js']['title']           = 'Заглавие';
$lang['js']['change']          = 'промени стандартите';
$lang['js']['insert']          = 'Bмъкни';
