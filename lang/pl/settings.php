<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Bartek S <sadupl@gmail.com>
 */
$lang['btns']                  = 'Pokaż przyciski skrótów na pasku narzędzi.';
$lang['cache']                 = 'Wyłącz pamięć podręczną.';
$lang['nums']                  = 'Pokaż numery lini.';
$lang['title']                 = 'Pokaż tytuł kodu.';
$lang['docstr']                = 'Pokaż docstring.';
