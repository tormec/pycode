<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Bartek S <sadupl@gmail.com>
 */
$lang['note']                  = 'Kod zmienił się od czasu ostatniej edycji tej strony.';
$lang['notfound-lns']          = 'Wiersze kodu, które próbujesz osadzić, są poza zasięgiem.';
$lang['notfound-def']          = 'Funkcja, którą próbujesz osadzić, nie istnieje.';
$lang['notfound-cls']          = 'Klasa, którą próbujesz osadzić, nie istnieje.';
$lang['wrong-flag']            = 'Flaga nie jest poprawna.';
$lang['wrong-lang']            = 'Plik, który próbujesz osadzić, nie jest skryptem w języku Python.';
$lang['error']                 = 'Błąd podczas otwierania kodu źródłowego.
Proszę sprawdzić ścieżkę do twojego repozytorium.';
$lang['pycode']                = 'Plugin PyCode';
$lang['title']                 = 'Nowy tytuł';
$lang['js']['wizard']          = 'Kreator PyCode';
$lang['js']['repository']      = 'Repozytorium';
$lang['js']['options']         = 'Opcje';
$lang['js']['embed']           = 'Osadzać';
$lang['js']['lns']             = 'linie';
$lang['js']['def']             = 'funkcja';
$lang['js']['cls']             = 'klasa';
$lang['js']['nums']            = 'Numery lini';
$lang['js']['docstr']          = 'Docstring';
$lang['js']['show']            = 'pokaż';
$lang['js']['hide']            = 'ukryj';
$lang['js']['title']           = 'Tytuł';
$lang['js']['change']          = 'zmień domyślne';
$lang['js']['insert']          = 'Wstaw';
