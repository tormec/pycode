<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author Jaroslav Lichtblau <jlichtblau@seznam.cz>
 */
$lang['note']                  = 'Od poslední úpravy této stránky se kód změnil.';
$lang['notfound-lns']          = 'Řádky kódu, který se snažíte vložit, jsou mimo rozsah.';
$lang['notfound-def']          = 'Funkce, kterou se snažíte vložit, neexistuje.';
$lang['notfound-cls']          = 'Třída, kterou se snažíte vložit, neexistuje.';
$lang['wrong-flag']            = 'Označení není správné.';
$lang['wrong-lang']            = 'Soubor, který se snažíte vložit, není Python skript.';
$lang['error']                 = 'Chyba otevření zdrojového kódu.
Ověřte prosím cestu ke svému repozitáři.';
$lang['pycode']                = 'Zásuvný modul PyCode';
$lang['title']                 = 'Nový nadpis';
