<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author Jaroslav Lichtblau <jlichtblau@seznam.cz>
 */
$lang['btns']                  = 'Zobrazit tlačítka zkratek v panelu.';
$lang['cache']                 = 'Vypnout cache.';
$lang['nums']                  = 'Zobrazit čísla řádek.';
$lang['title']                 = 'Zobrazit nadpis kódu.';
