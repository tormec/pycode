<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Alexandre Belchior <alexbelchior@gmail.com>
 */
$lang['note']                  = 'O código foi alterado desde a última vez que esta página foi editada.';
$lang['notfound-lns']          = 'As linhas de código que você está tentando incorporar estão fora do intervalo.';
$lang['notfound-def']          = 'A função que você está tentando incorporar não existe.';
$lang['notfound-cls']          = 'A classe que você está tentando incorporar não existe.';
$lang['wrong-flag']            = 'A flag não está correta.';
$lang['wrong-lang']            = 'O arquivo que você está tentando incorporar não é um script Python.';
$lang['error']                 = 'Falha ao abrir o código fonte.
Por favor, verifique o caminho do seu repositório.';
$lang['pycode']                = 'PyCode Plugin';
$lang['title']                 = 'Novo título';
$lang['js']['wizard']          = 'Assistente PyCode';
$lang['js']['repository']      = 'Repositório';
$lang['js']['options']         = 'Opções';
$lang['js']['embed']           = 'Incorporar';
$lang['js']['lns']             = 'linhas';
$lang['js']['def']             = 'função';
$lang['js']['cls']             = 'classe';
$lang['js']['nums']            = 'Números de linha';
$lang['js']['docstr']          = 'Docstring';
$lang['js']['show']            = 'mostrar';
$lang['js']['hide']            = 'ocultar';
$lang['js']['title']           = 'Título';
$lang['js']['change']          = 'mudar padrão';
$lang['js']['insert']          = 'Inserir';
