<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Alexandre Belchior <alexbelchior@gmail.com>
 */
$lang['btns']                  = 'Mostrar botões de atalho na barra de ferramentas.';
$lang['cache']                 = 'Desabilitar o cache.';
$lang['nums']                  = 'Exibir números de linha';
$lang['title']                 = 'Mostrar título do código.';
$lang['docstr']                = 'Mostrar docstring';
