<?php
/**
 * PyCode plugin: it embeds a Python script hosted in a remote repository.
 * 
 * lang.php: here are defined all the strings used by the PyCode plugin in 
 *      the DokuWiki pages.
 *
 * @author Torpedo <dgtorpedo@gmail.com>
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @package en_lang_strings
 */

$lang["note"] = "The code has changed since last time this page was edited.";
$lang["notfound-lns"] = "The lines of code you are trying to embed are out of range.";
$lang["notfound-def"] = "The function you are trying to embed doesn't exist.";
$lang["notfound-cls"] = "The class you are trying to embed doesn't exist.";
$lang["wrong-flag"] = "The flag is not correct.";
$lang["wrong-lang"] = "The file you are trying to embed isn't a Python script.";
$lang["error"] = "Failed to open the source code.
Please, check the path to your repo.";
$lang["pycode"] = "PyCode Plugin";
$lang["title"] = "New title";

$lang["js"]["wizard"] = "PyCode Wizard";
$lang["js"]["repository"] = "Repository";
$lang["js"]["options"] = "Options";
$lang["js"]["embed"] = "Embed";
$lang["js"]["lns"] = "lines";
$lang["js"]["def"] = "function";
$lang["js"]["cls"] = "class";
$lang["js"]["nums"] = "Line numbers";
$lang["js"]["docstr"] = "Docstring";
$lang["js"]["show"] = "show";
$lang["js"]["hide"] = "hide";
$lang["js"]["title"] = "Title";
$lang["js"]["change"] = "change default";
$lang["js"]["insert"] = "Insert";
