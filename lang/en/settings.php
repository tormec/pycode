<?php
/**
 * PyCode plugin: it embeds a Python script hosted in a remote repository.
 * 
 * settings.php: here are defined all the strings used by the PyCode plugin
 *      in Configuration Manager.
 * 
 * @author Torpedo <dgtorpedo@gmail.com>
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @package en_lang_settings
 */

$lang["btns"] = "Show shortcut buttons in the toolbar.";
$lang["cache"] = "Disable the cache.";
$lang["nums"] = "Show line numbers.";
$lang["title"] = "Show title of the code.";
$lang["docstr"] = "Show docstring.";
