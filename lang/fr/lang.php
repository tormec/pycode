<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Schplurtz le Déboulonné <schplurtz@laposte.net>
 * @author Nicolas Friedli <nicolas@theologique.ch>
 */
$lang['note']                  = 'Le code a changé depuis la dernière édition de la page.';
$lang['notfound-lns']          = 'Les lignes de code que vous essayez d\'intégrer son hors limites.';
$lang['notfound-def']          = 'La fonction que vous essayez d\'intégrer n\'existe pas.';
$lang['notfound-cls']          = 'La classe que vous essayez d\'intégrer n\'existe pas.';
$lang['wrong-flag']            = 'Le drapeau est incorrect.';
$lang['wrong-lang']            = 'Le fichier que vous essayez d\'intégrer n\'est pas un script Python.';
$lang['error']                 = 'Échec à l\'ouverture du code source.
Merci de vérifier le chemin d\'accès de votre dépôt.';
$lang['pycode']                = 'Plugin PyCode';
$lang['title']                 = 'Nouveau titre';
$lang['js']['wizard']          = 'Magicien PyCode';
$lang['js']['repository']      = 'Dépôt';
$lang['js']['options']         = 'Options';
$lang['js']['embed']           = 'Intégrer';
$lang['js']['lns']             = 'lignes';
$lang['js']['def']             = 'fonction';
$lang['js']['cls']             = 'classe';
$lang['js']['nums']            = 'Numéros de ligne';
$lang['js']['docstr']          = 'Docstring';
$lang['js']['show']            = 'montrer';
$lang['js']['hide']            = 'cacher';
$lang['js']['title']           = 'Titre';
$lang['js']['change']          = 'Changer les défauts';
$lang['js']['insert']          = 'Insérer';
