<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author Schplurtz le Déboulonné <schplurtz@laposte.net>
 * @author Nicolas Friedli <nicolas@theologique.ch>
 */
$lang['btns']                  = 'Montrer les boutons de raccourcis dans la barre d\'outils.';
$lang['cache']                 = 'Désactiver le cache.';
$lang['nums']                  = 'Montrer les numéros de ligne.';
$lang['title']                 = 'Montrer le titre du code.';
$lang['docstr']                = 'Montrer les docstrings.';
