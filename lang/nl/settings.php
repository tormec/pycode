<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author G. Uitslag <klapinklapin@gmail.com>
 */
$lang['btns']                  = 'Knop voor PyCode weergeven in de werkbalk van het bewerkvenster';
$lang['cache']                 = 'Cache uitschakelen.';
$lang['nums']                  = 'Regelnummers weergeven';
$lang['title']                 = 'Titel van de code weergeven.';
$lang['docstr']                = 'Docstring weergeven.';
