<?php

/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author G. Uitslag <klapinklapin@gmail.com>
 */
$lang['note']                  = 'De code is gewijzigd sinds de laatste keer dat de pagina is bewerkt.';
$lang['notfound-lns']          = 'De coderegels die je probeert in te bedden zijn buiten bereik.';
$lang['notfound-def']          = 'De functie die je probeert in te bedden bestaat niet.';
$lang['notfound-cls']          = 'De klasse die je probeert in te bedden bestaat niet.';
$lang['wrong-flag']            = 'De flag is niet correct.';
$lang['wrong-lang']            = 'Het bestand dat je probeert in te bedden is geen Python script.';
$lang['error']                 = 'Het openen van de broncode is mislukt.
Check alsjeblieft het pad naar je repository.';
$lang['pycode']                = 'PyCode Plugin';
$lang['title']                 = 'Nieuwe titel';
$lang['js']['wizard']          = 'PyCode Wizard';
$lang['js']['repository']      = 'Repository';
$lang['js']['options']         = 'Opties';
$lang['js']['embed']           = 'Ingebed';
$lang['js']['lns']             = 'regels';
$lang['js']['def']             = 'function';
$lang['js']['cls']             = 'class';
$lang['js']['nums']            = 'Regelnummers';
$lang['js']['docstr']          = 'Docstring';
$lang['js']['show']            = 'weergeven';
$lang['js']['hide']            = 'verbergen';
$lang['js']['title']           = 'Titel';
$lang['js']['change']          = 'wijzig standaardwaarde';
$lang['js']['insert']          = 'Invoegen';
